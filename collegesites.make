; Core version
; ------------

core = 7.x

; API version
; ------------

api = 2

; Core project
; ------------

projects[drupal][version] = "7.82"

; Base Modules
; ------------

projects[accordion_menu][download][type] = "git"
projects[accordion_menu][download][tag] = "7.x-1.3"
projects[accordion_menu][download][url] = "https://bitbucket.org/wwuweb/accordion_menu.git"
projects[accordion_menu][destination] = "modules"

projects[addressfield][version] = 1.3
projects[addressfield][type]= "module"

projects[adminimal_admin_menu][version] = 1.9
projects[adminimal_admin_menu][type] = "module"

projects[advagg][version] = 2.33
projects[advagg][type] = "module"

projects[advanced_help][version] = 1.5
projects[advanced_help][type] = "module"

projects[auto_entitylabel][version] = 1.4
projects[auto_entitylabel][type] = "module"
projects[auto_entitylabel][patch][] = "https://www.drupal.org/files/issues/work-with-ief-widget-2948845-2.patch"

projects[cas][version] = 1.7
projects[cas][type] = "module"

projects[calendar][version] = 3.5
projects[calendar][type] = "module"
projects[calendar][patch][] = "https://www.drupal.org/files/calendar-1545240-6.patch"

projects[ctools][version] = 1.15
projects[ctools][type] = "module"
projects[ctools][patch][] = "https://www.drupal.org/files/ctools-1405988.patch"

projects[date][version] = 2.10
projects[date][type] = "module"
projects[date][patch][] = "https://bitbucket.org/wwuweb/date-repeat-additional-patch/raw/ee3c09239da8dd4c2970b385a3c7d7ac7480b6b7/date-repeat-additional.patch"

projects[date_ical][version] = 3.9
projects[date_ical][type] = "module"

projects[diff][version] = 3.4
projects[diff][type] = "module"

projects[entity][version] = 1.9
projects[entity][type] = "module"

projects[entityreference][version] = 1.5
projects[entityreference][type] = "module"

projects[email][version] = 1.3
projects[email][type] = "module"

projects[feeds][version] = 2.0-beta4
projects[feeds][type] = "module"

projects[fences][version] = 1.2
projects[fences][type] = "module"

projects[file_entity][version] = 2.27
projects[file_entity][type] = "module"

projects[file_upload_security][version] = 3.5
projects[file_upload_security][type] = "module"

projects[flexifilter][version] = 1.x-dev
projects[flexifilter][type] = "module"

projects[flexslider][version] = 2.0-rc2
projects[flexslider][type] = "module"

projects[hierarchical_select][version] = 3.0-beta9
projects[hierarchical_select][type]= "module"

projects[httprl][version] = 1.14
projects[httprl][type] = "module"

projects[htmlpurifier][version] = 1.0
projects[htmlpurifier][type] = "module"

projects[image_resize_filter][version] = 1.16
projects[image_resize_filter][type]= "module"
projects[image_resize_filter][patch][] = "https://bitbucket.org/wwuweb/image-resize-filter-patch/raw/master/image_resize_filter_directory_path.patch"

projects[jquery_update][version] = 2.7
projects[jquery_update][type]= "module"

projects[job_scheduler][version] = 2.0
projects[job_scheduler][type]= "module"

projects[link][version] = 1.7
projects[link][type] = "module"

projects[linkchecker][version] = 1.4
projects[linkchecker][type]= "module"

projects[linkit][version] = 3.x-dev
projects[linkit][type] = "module"
projects[linkit][download][type] = git
projects[linkit][download][revision] = 542ad17

projects[media][version] = 2.26
projects[media][type] = "module"

projects[media_youtube][version] = 3.0
projects[media_youtube][type] = "module"

projects[media_vimeo][version] = 2.1
projects[media_vimeo][type] = "module"

projects[oauth][version] = 3.4
projects[oauth][type] = "module"

projects[oembed][version] = 1.x-dev
projects[oembed][type] = "module"
projects[oembed][download][type] = git
projects[oembed][download][revision] = 1664b19
projects[oembed][patch][] = "https://www.drupal.org/files/issues/split-up-regex-2739023-1.patch"

projects[og][version] = 2.10
projects[og][type] = "module"

;projects[og_vocab][version] = 1.1
;projects[og_vocab][type] = "module"

projects[pathauto][version] = 1.3
projects[pathauto][type] = "module"

projects[pathologic][version] = 2.12
projects[pathologic][type] = "module"

projects[panels][version] = 3.9
projects[panels][type] = "module"

projects[phone][version] = 1.0-beta1
projects[phone][type] = "module"

projects[token][version] = 1.7
projects[token][type] = "module"

projects[role_delegation][version] = 1.2
projects[role_delegation][type] = "module"

projects[wysiwyg][version] = 2.x-dev
projects[wysiwyg][type] = "module"
projects[wysiwyg][download][type] = git
projects[wysiwyg][download][revision] = 1f522090

projects[views][version] = 3.23
projects[views][type] = "module"

projects[views_bulk_operations][version] = 3.5
projects[views_bulk_operations][type] = "module"

projects[wwu_tsa][download][type] = "git"
projects[wwu_tsa][download][tag] = "7.x-1.3"
projects[wwu_tsa][download][url] = "https://bitbucket.org/wwuweb/wwu_tsa.git"
projects[wwu_tsa][type] = "module"

; Ancilliary Modules
; ------------------

projects[addtoany][version] = 4.16
projects[addtoany][type] = "module"

projects[admin_views][version]= 1.7
projects[admin_views][type] = "module"

projects[addanother][version] = 2.2
projects[addanother][type] = "module"

projects[admin_menu][version] = 3.0-rc6
projects[admin_menu][type] = "module"

projects[ajaxblocks][version] = 1.4
projects[ajaxblocks][type] = "module"

projects[backup_migrate][version] = 3.6
projects[backup_migrate][type] = "module"

projects[breakpoints][version] = 1.6
projects[breakpoints][type] = "module"

projects[captcha][version] = 1.7
projects[captcha][type] = "module"

projects[clientside_validation][version] = 1.47
projects[clientside_validation][type] = "module"

projects[colorbox][version] = 2.13
projects[colorbox][type] = "module"

projects[clean_markup][version] = 2.9
projects[clean_markup][type] = "module"

projects[conditional_fields][version] = 3.0-alpha2
projects[conditional_fields][type] = "module"

projects[drafty][version] = 1.0-rc1
projects[drafty][type] = "module"

projects[draggableviews][version] = 2.1
projects[draggableviews][type] = "module"

projects[edit_profile][version] = 1.0-beta2
projects[edit_profile][type] = "module"

projects[elements][version] = 1.5
projects[elements][type] = "module"

projects[entity_view_mode][version] = 1.0-rc1
projects[entity_view_mode][type] = "module"

projects[environment_indicator][version] = 2.9
projects[environment_indicator][type] = "module"

projects[exclude_node_title][version] = 1.9
projects[exclude_node_title][type] = "module"

projects[facebook_pull][version] = 3.5
projects[facebook_pull][type] = "module"

projects[fast_token_browser][version] = 1.5
projects[fast_token_browser][type] = "module"

projects[fpa][version] = 2.6
projects[fpa][type] = "module"

projects[features][version] = 2.11
projects[features][type] = "module"

projects[feeds_ex][version] = 1.0-beta2
projects[feeds_ex][type] = "module"

projects[feeds_tamper][version] = 1.2
projects[feeds_tamper][type] = "module"

projects[field_collection][version] = 1.1
projects[field_collection][type] = "module"

projects[field_group][version] = 1.6
projects[field_group][type] = "module"

projects[focal_point][version] = 1.2
projects[focal_point][type] = "module"

projects[form_builder][version] = 1.22
projects[form_builder][type] = "module"

projects[globalredirect][version] = 1.6
projects[globalredirect][type] = "module"

projects[google_analytics][version] = 2.6
projects[google_analytics][type] = "module"

projects[hidden_nodes][version] = 1.5
projects[hidden_nodes][type] = "module"

projects[ldap][version] = 2.5
projects[ldap][type] = "module"

projects[libraries][version] = 2.5
projects[libraries][type] = "module"

projects[mailchimp][version] = 5.6
projects[mailchimp][type] = "module"
projects[mailchimp][patch][] = "https://bitbucket.org/wwuweb/mailchimp-lists-patch/raw/8e2c8a488f9168b7a2e043d0b63606aa0250d727/mailchimp-lists-update-hook.patch"

projects[mailsystem][version] = 2.35
projects[mailsystem][type] = "module"

projects[media_browser_plus][version] = 3.0-beta4
projects[media_browser_plus][type] = "module"

projects[menu_admin_per_menu][version] = 1.1
projects[menu_admin_per_menu][type] = "module"

projects[mimemail][version] = 1.1
projects[mimemail][type] = "module"

projects[module_filter][version] = 2.2
projects[module_filter][type] = "module"

projects[menu_attributes][version] = 1.0
projects[menu_attributes][type] = "module"

projects[menu_block][version] = 2.7
projects[menu_block][type] = "module"
projects[menu_block][patch][] = "https://www.drupal.org/files/issues/menu_block-7.x-2.7.patch"

projects[metatag][version] = 1.27
projects[metatag][type] = "module"

projects[multiform][version] = 1.6
projects[multiform][type] = "module"

projects[node_clone][version] = 1.0
projects[node_clone][type] = "module"

projects[node_embed][version] = 1.2
projects[node_embed][type] = "module"
projects[node_embed][patch][] = "https://www.drupal.org/files/issues/recoverable_fatal-2464391-2.patch"

projects[nodequeue][version] = 2.2
projects[nodequeue][type] = "module"

projects[og_menu][version] = 3.2
projects[og_menu][type] = "module"

projects[og_webform][version] = 1.0-beta1
projects[og_webform][type] = "module"
projects[og_webform][patch][] = "http://cgit.drupalcode.org/og_webform/patch/?id=b60f03ae4de8050bb2499106484df085b9884b25"
projects[og_webform][patch][] = "https://www.drupal.org/files/og_webform_api2-1946432_0.patch"

projects[options_element][version] = 1.12
projects[options_element][type] = "module"

projects[picture][version] = 2.13
projects[picture][type] = "module"

projects[plupload][version] = 1.7
projects[plupload][type] = "module"

projects[quicktabs][version] = 3.8
projects[quicktabs][type] = "module"
projects[quicktabs][patch][] = "https://www.drupal.org/files/issues/quicktabs-tab-history-1454486-35.patch"

projects[recaptcha][version] = 2.3
projects[recaptcha][type] = "module"

projects[redirect][version] = 1.0-rc3
projects[redirect][type] = "module"

projects[rules][version] = 2.12
projects[rules][type] = "module"

projects[r4032login][version] = 1.8
projects[r4032login][type] = "module"

projects[select_or_other][version] = 2.24
projects[select_or_other][type] = "module"

projects[simplify][version] = 3.4
projects[simplify][type] = "module"

projects[taxonomy_unique][version] = 1.0
projects[taxonomy_unique][type] = "module"

projects[term_merge][version] = 1.4
projects[term_merge][type] = "module"

projects[themekey][version] = 3.4
projects[themekey][type] = "module"

projects[token_filter][version] = 1.1
projects[token_filter][type] = "module"

projects[twitter][version] = 5.11
projects[twitter][type] = "module"
projects[twitter][subdir] = contrib

projects[vbo_search_and_replace][version] = 1.1
projects[vbo_search_and_replace][type] = "module"

projects[views_accordion][version] = 1.2
projects[views_accordion][type] = "module"

projects[viewfield][version] = 2.1
projects[viewfield][type] = "module"

projects[views_field_view][version] = 1.2
projects[views_field_view][type] = "module"

projects[views_datasource][version] = 1.0-alpha3
projects[views_datasource][type] = "module"

projects[views_responsive_grid][version] = 1.3
projects[views_responsive_grid][type] = "module"

projects[views_tree][version] = 2.x-dev
projects[views_tree][type] = "module"
projects[views_tree][download][type] = git
projects[views_tree][download][revision] = 7dd712

projects[webform][version] = 4.22
projects[webform][type] = "module"

projects[webform_scheduler][version] = 1.0-beta9
projects[webform_scheduler][type] = "module"

projects[webform_validation][version] = 1.17
projects[webform_validation][type] = "module"

projects[webform_rules][version] = 1.6
projects[webform_rules][type] = "module"
projects[webform_rules][patch][] = "https://www.drupal.org/files/issues/webform_rules-expose_submission_data-2020149-56.patch"
projects[webform_rules][patch][] = "https://www.drupal.org/files/issues/webform_rules-expose_submission_data-2020149-27.patch"
projects[webform_rules][patch][] = "https://www.drupal.org/files/issues/webform_rules-fix_for_conditional_fail-2554435-5.patch"

projects[weight][version] = 2.5
projects[weight][type] = "module"

projects[workbench][version] = 1.2
projects[workbench][type] = "module"

projects[workbench_access][version] = 1.6
projects[workbench_access][type] = "module"

projects[workbench_email][version] = 3.12
projects[workbench_email][type] = "module"

projects[workbench_media][version] = 2.1
projects[workbench_media][type] = "module"

projects[workbench_moderation][version] = 3.0
projects[workbench_moderation][type] = "module"

projects[xmlsitemap][version] = 2.6
projects[xmlsitemap][type] = "module"

projects[wwu_ckeditor_plugins][download][type] = "git"
projects[wwu_ckeditor_plugins][download][url] = "https://bitbucket.org/wwuweb/wwu_ckeditor_plugins.git"
projects[wwu_ckeditor_plugins][type] = "module"

projects[wwu_ckeditor_settings][download][type] = "git"
projects[wwu_ckeditor_settings][download][url] = "https://bitbucket.org/wwuweb/wwu_ckeditor_settings.git"
projects[wwu_ckeditor_settings][type] = "module"

projects[wwu_clean_file_urls][download][type] = "git"
projects[wwu_clean_file_urls][download][tag] = "7.x-1.3"
projects[wwu_clean_file_urls][download][url] = "https://bitbucket.org/wwuweb/wwu_clean_file_urls.git"
projects[wwu_clean_file_urls][type] = "module"

projects[wwu_greek][download][type] = "git"
projects[wwu_greek][download][url] = "https://bitbucket.org/wwuweb/wwu_greek.git"
projects[wwu_greek][type] = "module"

projects[wwu_img_caption][download][type] = "git"
projects[wwu_img_caption][download][tag] = "7.x-1.6"
projects[wwu_img_caption][download][url] = "https://bitbucket.org/wwuweb/wwu_img_caption.git"
projects[wwu_img_caption][type] = "module"

projects[wwu_mediafix][download][type] = "git"
projects[wwu_mediafix][download][tag] = "7.x-1.0"
projects[wwu_mediafix][download][url] = "https://bitbucket.org/wwuweb/wwu_mediafix.git"
projects[wwu_mediafix][type] = "module"

projects[wwu_scayt_extension][download][type] = "git"
projects[wwu_scayt_extension][download][url] = "https://bitbucket.org/wwuweb/wwu_scayt_extension.git"
projects[wwu_scayt_extension][type] = "module"

projects[wwu_rules_extensions][download][type] = "git"
projects[wwu_rules_extensions][download][tag] = "7.x-1.0"
projects[wwu_rules_extensions][download][url] = "https://bitbucket.org/wwuweb/wwu_rules_extensions.git"
projects[wwu_rules_extensions][type] = "module"

projects[wysiwyg_tools_plus][version] = 1.0-beta1
projects[wysiwyg_tools_plus][type] = "module"


; Themes
; --------

projects[adminimal_theme][version] = 1.26
projects[adminimal_theme][type] = "theme"

projects[zen][version] = 5.6
projects[zen][type] = "theme"

projects[wwuzen][download][type] = "git"
projects[wwuzen][download][url] = "https://bitbucket.org/wwuweb/wwuzen.git"
projects[wwuzen][type] = "theme"

projects[wwuadminimal][download][type] = "git"
projects[wwuadminimal][download][url] = "https://bitbucket.org/wwuweb/wwuadminimal.git"
projects[wwuadminimal][type] = "theme"


; Libraries
; ---------

; The cas module requires the library directory to be all uppercase
libraries[CAS][download][type] = "file"
libraries[CAS][download][url] = "https://github.com/apereo/phpCAS/archive/1.3.5.zip"
libraries[CAS][download][subtree] = phpCAS-1.3.5/

libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.16.1/ckeditor_4.16.1_full.zip"

libraries[widget][download][type] = "file"
libraries[widget][download][url] = "https://download.ckeditor.com/widget/releases/widget_4.12.1.zip"
libraries[widget][destination] = "/libraries/ckeditor/plugins"

libraries[lineutils][download][type] = "file"
libraries[lineutils][download][url] = "https://download.ckeditor.com/lineutils/releases/lineutils_4.12.1.zip"
libraries[lineutils][destination] = "/libraries/ckeditor/plugins"

libraries[colorbox][download][type] = "file"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/1.6.4.zip"

libraries[flexslider][download][type] = "file"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider/archive/version/2.2.2.zip"

libraries[jquery][download][type] = "file"
libraries[jquery][download][url] = "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"

libraries[jqueryui][download][type] = "file"
libraries[jqueryui][download][url] = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"

libraries[mailchimp][download][type] = "get"
libraries[mailchimp][download][url] = "https://github.com/thinkshout/mailchimp-api-php/releases/download/v1.0.10/v1.0.10-package.zip"
libraries[mailchimp][directory_name] = "mailchimp"
libraries[mailchimp][destination] = "libraries"

; For use by feeds module
;libraries[simplepie][download][type] = "file"
;libraries[simplepie][download][url] = "https://github.com/simplepie/simplepie/archive/1.3.1.tar.gz"
;libraries[simplepie][download][subtree] = /library
;libraries[simplepie][destination] = "sites/all/modules/feeds/libraries"

libraries[simple_html_dom][download][type] = "file"
libraries[simple_html_dom][download][url] = "https://bitbucket.org/wwuweb/simple_html_dom/get/master.zip"
libraries[simple_html_dom][directory_name] = "simple_html_dom"


; Features
; --------

libraries[gallery][download][type] = "git"
libraries[gallery][download][tag] = "7.x-1.3"
libraries[gallery][download][url] = "https://bitbucket.org/wwuweb/gallery.git"
libraries[gallery][destination] = "modules"

libraries[organizational_unit][download][type] = "git"
libraries[organizational_unit][download][tag] = "7.x-1.2"
libraries[organizational_unit][download][url] = "https://bitbucket.org/wwuweb/organizational_unit.git"
libraries[organizational_unit][destination] = "modules"

libraries[slideshow][download][type] = "git"
libraries[slideshow][download][tag] = "7.x-1.0"
libraries[slideshow][download][url] = "https://bitbucket.org/wwuweb/slideshow.git"
libraries[slideshow][destination] = "modules"

libraries[staff_and_faculty_profiles_and_directory][download][type] = "git"
libraries[staff_and_faculty_profiles_and_directory][download][tag] = "7.x-3.0"
libraries[staff_and_faculty_profiles_and_directory][download][url] = "https://bitbucket.org/wwuweb/staff_and_faculty_profiles_and_directory.git"
libraries[staff_and_faculty_profiles_and_directory][destination] = "modules"

libraries[wwu_events][download][type] = "git"
libraries[wwu_events][download][tag] = "7.x-1.2"
libraries[wwu_events][download][url] = "https://bitbucket.org/wwuweb/wwu_events.git"
libraries[wwu_events][destination] = "modules"

libraries[wwu_faq][download][type] = "git"
libraries[wwu_faq][download][tag] = "7.x-1.0"
libraries[wwu_faq][download][url] = "https://bitbucket.org/wwuweb/wwu_faq.git"
libraries[wwu_faq][destination] = "modules"

libraries[wwu_fields][download][type] = "git"
libraries[wwu_fields][download][tag] = "7.x-2.2"
libraries[wwu_fields][download][url] = "https://bitbucket.org/wwuweb/wwu_fields.git"
libraries[wwu_fields][destination] = "modules"

libraries[wwu_ldap_server][download][type] = "git"
libraries[wwu_ldap_server][download][tag] = "7.x-1.0"
libraries[wwu_ldap_server][download][url] = "https://bitbucket.org/wwuweb/wwu_ldap_server.git"
libraries[wwu_ldap_server][destination] = "modules"

libraries[wwu_linkit_profile][download][type] = "git"
libraries[wwu_linkit_profile][download][tag] = "7.x-1.1"
libraries[wwu_linkit_profile][download][url] = "https://bitbucket.org/wwuweb/wwu_linkit_profile.git"
libraries[wwu_linkit_profile][destination] = "modules"

libraries[wwu_roles_permissions][download][type] = "git"
libraries[wwu_roles_permissions][download][tag] = "7.x-2.2"
libraries[wwu_roles_permissions][download][url] = "https://bitbucket.org/wwuweb/wwu_roles_permissions.git"
libraries[wwu_roles_permissions][destination] = "modules"

libraries[wwu_student_profiles][download][type] = "git"
libraries[wwu_student_profiles][download][tag] = "7.x-1.1"
libraries[wwu_student_profiles][download][url] = "https://bitbucket.org/wwuweb/wwu_student_profiles.git"
libraries[wwu_student_profiles][destination] = "modules"
